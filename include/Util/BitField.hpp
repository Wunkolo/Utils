#pragma once
#include <cstdint>
#include <cstddef>

namespace Util
{
namespace Bits
{
template< std::size_t Index, std::size_t BitCount, typename UnderlyingType >
class BitField
{
public:
	BitField()
		: Field(0)
	{
	}

	BitField(UnderlyingType Value)
		: Field(Value)
	{
	}

	operator UnderlyingType() const
	{
		return (Field >> Index) & Mask;
	}

	explicit operator bool() const
	{
		return (Field & (Mask << Index)) != UnderlyingType(0);
	}

	BitField& operator++()
	{
		return Field += UnderlyingType(1);
	}

	BitField& operator--()
	{
		return Field -= UnderlyingType(1);
	}

	BitField operator++(int)
	{
		return Field++;
	}

	BitField operator--(int)
	{
		return Field++;
	}

	BitField& operator=(UnderlyingType Value)
	{
		Field = (Field & ~(Mask << Index)) | ((Value & Mask) << Index);
		return *this;
	}

private:
	UnderlyingType Field;
	constexpr static UnderlyingType Mask = (UnderlyingType(1) << BitCount) - UnderlyingType(1);
};

template< std::size_t Index, std::size_t BitCount = 1 >
using BitField8 = BitField<Index, BitCount, std::uint8_t>;

template< std::size_t Index, std::size_t BitCount = 1 >
using BitField16 = BitField<Index, BitCount, std::uint16_t>;

template< std::size_t Index, std::size_t BitCount = 1 >
using BitField32 = BitField<Index, BitCount, std::uint32_t>;

template< std::size_t Index, std::size_t BitCount = 1 >
using BitField64 = BitField<Index, BitCount, std::uint64_t>;
}
}
