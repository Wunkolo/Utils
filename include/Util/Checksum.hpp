#pragma once

#include <cstdint>
#include <cstddef>

namespace Util
{
namespace Checksum
{
std::uint16_t Crc16(const void* Data, std::size_t Length, std::uint16_t PrevCrc = 0);
std::uint32_t Crc32(const void* Data, std::size_t Length, std::uint32_t PrevCrc = 0);

std::uint16_t Fletcher16(const void* Data, std::size_t Length);

std::uint16_t Bsd16(const void* Data, std::size_t Length);

std::uint32_t Fnv32(const void* Data, std::size_t Length);
std::uint32_t FnvA32(const void* Data, std::size_t Length);

std::uint64_t Fnv64(const void* Data, std::size_t Length);
std::uint64_t FnvA64(const void* Data, std::size_t Length);

std::uint32_t Murmur32(const void* Data, std::uint32_t Length, std::uint32_t Seed = 0);
}
}
