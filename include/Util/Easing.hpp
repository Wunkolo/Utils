#pragma once
#include <type_traits>

namespace Util
{
namespace Easing
{
template< typename T, typename N >
inline T Lerp(const T& A, const T& B, N t)
{
	return T(A * (N(1) - t)) + T(t * B);
}

template< typename T >
inline T SmoothStep(T t)
{
	return t * t * (T(3) - T(2) * t);
}

template< typename T >
inline T SmootherStep(T t)
{
	return t * t * t * (t * (t * T(6) - T(15)) + T(10));
}

template< typename T, typename N >
inline T Hermite(
	const T& A, N Tan1,
	const T& B, N Tan2,
	N t)
{
	N Pow3 = t * t * t;
	N Pow2 = t * t;
	return
		T(
			((N(2) * Pow2 - N(3) * Pow2) * A)
			+ ((Pow3 - N(2) * Pow2 + t) * Tan1)
			+ ((-N(2) * Pow3 + N(3) * Pow2) * B)
			+ (Pow3 - Pow2) * Tan2
		);
}

template< typename T, typename N >
inline T CatmullRom(
	const T& A1, const T& A2,
	const T& B1, const T& B2,
	N t
)
{
	return Hermite<T, N>(
		A1, N(0.5) * (A1 + A2),
		B1, N(0.5) * (B1 + B2));
}

template< typename T, typename N >
inline T BezierQuad(
	const T& A,
	const T& B,
	const T& C,
	N t
)
{
	N tInv = (N(1) - t);
	return
		T(
			(tInv * tInv) * A
			+ N(2) * tInv * t * B
			+ (t * t) * C
		);
}

template< typename T, typename N >
inline T BezierCube(
	const T& A,
	const T& B,
	const T& C,
	const T& D,
	N t
)
{
	N tInv = (N(1) - t);
	N ttInv = tInv * tInv;
	N tt = t * t;
	return
		T(
			ttInv * tInv * A
			+ N(3) * ttInv * t * B
			+ N(3) * ttInv * tt * C
			+ (tt * t) * D
		);
}
}
}
