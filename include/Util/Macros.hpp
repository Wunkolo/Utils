#pragma once

#include <inttypes.h>
#include <type_traits>

namespace Util
{
// array[5]
// GetArraySize(array) = 5
template< typename T, std::size_t Num >
inline std::size_t GetArraySize(T (&a)[Num])
{
	return Num;
}

template< typename T >
inline T Abs(T n)
{
	return (n >= 0) ? n : -n;
}

template< typename T >
inline T Max(T a, T b)
{
	return (a >= b) ? a : b;
}

template< typename T >
inline T Min(T a, T b)
{
	return (a <= b) ? a : b;
}

template< typename T >
inline T Clamp(T n, T low, T high)
{
	return (n >= high) ? high : ((n <= low) ? low : n);
}
}

// Development Macros

#define STRINGIZE_AGAIN( MSG ) #MSG
#define STRINGIZE( MSG ) STRINGIZE_AGAIN( MSG )

#if defined(_MSC_VER)
#define NOTE( MESSAGE ) \
  __pragma(message(__FILE__ "(" STRINGIZE(__LINE__) "): Note : " MESSAGE " "))

#define WARN( MESSAGE ) \
  __pragma(message(__FILE__ "(" STRINGIZE(__LINE__) "): Warning : " MESSAGE " "))

#define TODO( MESSAGE ) \
  __pragma(message(__FILE__ "(" STRINGIZE(__LINE__) "): Todo : " MESSAGE " "))

#else

#define NOTE( MESSAGE ) \
  _Pragma( STRINGIZE(message  __FILE__ "(" STRINGIZE(__LINE__) "): NOTE : " MESSAGE " "))

#define WARN( MESSAGE ) \
  _Pragma( STRINGIZE(message  __FILE__ "(" STRINGIZE(__LINE__) "): Warning : " MESSAGE " "))

#define TODO( MESSAGE ) \
  _Pragma( STRINGIZE(message  __FILE__ "(" STRINGIZE(__LINE__) "): Todo : " MESSAGE " "))

#endif
