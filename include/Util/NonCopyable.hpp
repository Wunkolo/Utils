#pragma once

namespace Util
{
namespace Trait
{
template< typename T >
class NonCopyable
{
public:
	NonCopyable(const NonCopyable&) = delete;
	NonCopyable& operator=(const NonCopyable&) = delete;
};
}
}
