#pragma once
#include <cstdint>
#include <cstddef>
#include <cstring>
#include <algorithm>

namespace Util
{
class Pointer
{
public:
	constexpr Pointer()
		:
		Address(0)
	{
	};

	explicit constexpr Pointer(void* Pointer)
		:
		Address(
			reinterpret_cast<std::uintptr_t>(Pointer)
		)
	{
	}

	explicit constexpr Pointer(std::uintptr_t Address)
		: Address(Address)
	{
	}

	inline std::uintptr_t GetAddress() const
	{
		return Address;
	}

	inline void SetAddress(const std::uintptr_t Address)
	{
		this->Address = Address;
	}

	inline Pointer operator[](const std::ptrdiff_t Offset) const
	{
		return Pointer(Pointer(Address + Offset).Read<std::uintptr_t>());
	}

	inline Pointer operator()(const std::ptrdiff_t Offset) const
	{
		return Pointer(Address + Offset);
	}

	inline Pointer operator+(std::ptrdiff_t Offset) const
	{
		return Pointer(Address + Offset);
	}

	inline Pointer operator-(std::ptrdiff_t Offset) const
	{
		return Pointer(Address + Offset);
	}

	bool operator==(const decltype(nullptr)) const
	{
		return reinterpret_cast<void*>(this->Address) == nullptr;
	}

	bool operator!=(const decltype(nullptr)) const
	{
		return reinterpret_cast<void*>(this->Address) != nullptr;
	}

	inline operator std::uintptr_t() const
	{
		return Address;
	}

	template< typename T = void >
	inline T* Point() const
	{
		return reinterpret_cast<T*>(Address);
	}

	// Templated Read/Write

	template< typename T >
	inline T& Get() const
	{
		return *Point<T>();
	}

	template< typename T >
	inline const T& Read() const
	{
		return Get<T>();
	}

	inline void Read(void* Destination, std::size_t Count) const
	{
		std::memcpy(
			Destination,
			Point(),
			Count
		);
	}

	template< typename T >
	inline void Write(const T& Data) const
	{
		Get<T>() = Data;
	}

	inline void Write(const void* Source, std::size_t Count) const
	{
		std::memcpy(
			Point(),
			Source,
			Count
		);
	}

	template< typename T >
	void Fill(const T& Data, const std::size_t Count)
	{
		std::fill_n(Point<T>(), Count, Data);
	}

private:
	std::uintptr_t Address;
};
}
