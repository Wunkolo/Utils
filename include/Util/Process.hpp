#pragma once
#include <Util/Pointer.hpp>

#include <type_traits>

namespace Util
{
namespace Process
{
std::int32_t GetProcessID();

// Resolves current process base address and caches it for later re-use
Pointer GetProcessBase();

// Resolves base address by module name(exe or dll file)
Pointer GetModuleBase(const char* ModuleName);

// Default exe base address for current architecture
// 0x140000000 for x64
// 0x400000    for x32
constexpr Pointer Base = std::conditional<
	sizeof(void*) == 8,
	std::integral_constant<std::uintptr_t, 0x140000000>,
	std::integral_constant<std::uintptr_t, 0x400000>
>::type::value;

// Default dll base address for current architecture
// 0x180000000 for x64
// 0x10000000  for x32
constexpr Pointer BaseDLL = std::conditional<
	sizeof(void*) == 8,
	std::integral_constant<std::uintptr_t, 0x180000000>,
	std::integral_constant<std::uintptr_t, 0x10000000>
>::type::value;

// Return false to stop iteration
using ModuleCallback = bool(*)(
	const char* Name,
	const char* Path,
	Pointer Base,
	std::size_t Size
);

// Iterates all modules of the current process
void IterateModules(
	ModuleCallback ModuleProc,
	std::uint32_t ProcessID = GetProcessID()
);

// Return false to stop iteration
using MemoryCallback = bool(*)(
	Pointer Base,
	std::size_t Size,
	std::uint32_t Type,
	std::uint32_t State,
	std::uint32_t Protection
);

// Iterates all readable memory in the current process
void IterateReadableMemory(
	MemoryCallback MemoryProc
);
}
}
