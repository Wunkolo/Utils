#pragma once

namespace Util
{
namespace Trait
{
template< typename T >
class Singleton
{
public:
	static T& Instance()
	{
		static T Inst;
		return Inst;
	}

	Singleton(Singleton const&) = delete;
	Singleton(Singleton&&) = delete;
	Singleton& operator=(Singleton const&) = delete;
	Singleton& operator=(Singleton&&) = delete;
protected:
	Singleton()
	{
	};

	~Singleton()
	{
	};
};
}
}
