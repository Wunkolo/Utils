#pragma once

#include <string>
#include <vector>

namespace Util
{
namespace String
{
void Replace(std::string& String, char Find, char Replace);

std::wstring WidenString(const std::string& String);

std::string ThinString(const std::wstring& String);

std::string ToLower(const std::string& String);

std::vector<std::string> SplitString(const std::string& String, char Delimiter);

// Trim from start
std::string& TrimL(std::string& String);

// Trim from end
std::string& TrimR(std::string& String);

// Trim from both ends
inline std::string& Trim(std::string& String)
{
	return TrimL(TrimR(String));
}

std::string Trim(const std::string& String, bool Reverse);

std::vector<std::string> Wrap(const std::string& String, std::size_t LineLength);

std::size_t Levenshtein(const std::string& Operand1, const std::string& Operand2);
}
}
