#pragma once

#include <cstdint>

#include "Pointer.hpp"
#include "Process.hpp"

namespace Util
{
namespace Thread
{
// Gets Thread ID of the currently executing thing
std::uintmax_t GetCurrentThreadId();

// Returns a pointer to the allocated TLS slot for the specified thread ID
// Returns null on failure
Pointer GetThreadLocalStorage(std::uint32_t ThreadID, std::size_t TlsSlot);

// Return false to stop iteration
using ThreadCallback = bool(*)(
	std::uint32_t ThreadID
);

// Iterates all threads of the Specified process ID
void IterateThreads(
	ThreadCallback ThreadProc,
	std::uint32_t ProcessID = Process::GetProcessID()
);
}
}
