#include <Util/Checksum.hpp>

namespace Util
{
namespace Checksum
{
std::uint16_t Crc16(const void* Data, std::size_t Length, std::uint16_t PrevCrc)
{
	const std::uint16_t Polynomial = 0x8408;
	std::uint16_t Crc = ~PrevCrc;
	const std::uint8_t* Current = static_cast<const std::uint8_t*>(Data);
	while( Length-- )
	{
		Crc ^= *Current++;
		for( std::size_t i = 0; i < 8; i++ )
		{
			Crc = (Crc >> 1) ^ (-std::int16_t(Crc & 1) & Polynomial);
		}
	}
	return ~Crc;
}

std::uint32_t Crc32(const void* Data, std::size_t Length, std::uint32_t PrevCrc)
{
	const std::uint32_t Polynomial = 0xEDB88320;
	std::uint32_t Crc = ~PrevCrc;
	const std::uint8_t* Current = static_cast<const std::uint8_t*>(Data);
	while( Length-- )
	{
		Crc ^= *Current++;
		for( std::size_t i = 0; i < 8; i++ )
		{
			Crc = (Crc >> 1) ^ (-std::int32_t(Crc & 1) & Polynomial);
		}
	}

	return ~Crc;
}

std::uint16_t Fletcher16(const void* Data, std::size_t Length)
{
	std::uint16_t Sum1;
	std::uint16_t Sum2;
	Sum1 = Sum2 = 0;
	for( std::size_t i = 0; i < Length; ++i )
	{
		Sum1 += static_cast<const std::uint8_t*>(Data)[i];
		Sum1 %= 0xff;
		Sum2 += Sum1;
		Sum2 %= 0xff;
	}
	return (Sum2 << 8) | Sum1;
}

std::uint16_t Bsd16(const void* Data, std::size_t Length)
{
	std::uint16_t Checksum = 0;
	for( std::size_t i = 0; i < Length; i++ )
	{
		Checksum = (Checksum >> 1) | ((Checksum & 1) << 15);
		Checksum += static_cast<const std::uint8_t*>(Data)[i];
		Checksum &= 0xffff;
	}
	return Checksum;
}

std::uint32_t Fnv32(const void* Data, std::size_t Length)
{
	const std::uint32_t OffsetBasis = 2166136261u;
	const std::uint32_t Prime = 16777619u;

	std::uint32_t Checksum = OffsetBasis;
	for( std::size_t i = 0; i < Length; i++ )
	{
		Checksum *= Prime;
		Checksum ^= static_cast<const std::uint8_t*>(Data)[i];
	}
	return Checksum;
}

std::uint32_t FnvA32(const void* Data, std::size_t Length)
{
	const std::uint32_t OffsetBasis = 2166136261u;
	const std::uint32_t Prime = 16777619u;

	std::uint32_t Checksum = OffsetBasis;
	for( std::size_t i = 0; i < Length; i++ )
	{
		Checksum ^= static_cast<const std::uint8_t*>(Data)[i];
		Checksum *= Prime;
	}
	return Checksum;
}

std::uint64_t Fnv64(const void* Data, std::size_t Length)
{
	const std::uint64_t OffsetBasis = 14695981039346656037u;
	const std::uint64_t Prime = 1099511628211u;

	std::uint64_t Checksum = OffsetBasis;
	for( std::size_t i = 0; i < Length; i++ )
	{
		Checksum *= Prime;
		Checksum ^= static_cast<const std::uint8_t*>(Data)[i];
	}
	return Checksum;
}

std::uint64_t FnvA64(const void* Data, std::size_t Length)
{
	const std::uint64_t OffsetBasis = 14695981039346656037u;
	const std::uint64_t Prime = 1099511628211u;

	std::uint64_t Checksum = OffsetBasis;
	for( std::size_t i = 0; i < Length; i++ )
	{
		Checksum ^= static_cast<const std::uint8_t*>(Data)[i];
		Checksum *= Prime;
	}
	return Checksum;
}

std::uint32_t Murmur32(const void* Data, std::uint32_t Length, std::uint32_t Seed)
{
	static const std::uint32_t C1 = 0xCC9E2D51;
	static const std::uint32_t C2 = 0x1B873593;
	static const std::uint32_t R1 = 15;
	static const std::uint32_t R2 = 13;
	static const std::uint32_t M = 5;
	static const std::uint32_t N = 0xE6546B64;
	std::uint32_t K1 = 0;

	std::uint32_t Hash = Seed;
	std::uint32_t Chunks = Length >> 2;

	// Process all 4 byte chunks
	for( std::size_t i = K1 = 0; i < Chunks; i++ )
	{
		std::uint32_t K = static_cast<const std::uint32_t*>(Data)[i];
		K *= C1;
		K = (K << R1) | (K >> (32 - R1));
		K *= C2;

		Hash ^= K;
		Hash = ((Hash << R2) | (Hash >> (32 - R2)));
		Hash *= M + N;
	}

	// Remaining bytes
	const std::uint32_t* End = static_cast<const std::uint32_t*>(Data) + Chunks * 4;

	switch( Length & 3 )
	{
	case 3:
	{
		Hash ^= End[2] << 16;
		break;
	}
	case 2:
	{
		K1 ^= End[1] << 8;
		break;
	}
	case 1:
	{
		K1 ^= End[0];
		K1 = (K1 << R1) | (K1 >> (32 - R1));
		K1 *= C2;
		Hash ^= K1;
		break;
	}
	default:
		break;
	}

	Hash ^= Length;
	Hash ^= (Hash >> 16);
	Hash *= 0x85EBCA6B;
	Hash ^= (Hash >> 13);
	Hash *= 0xC2B2AE35;
	Hash ^= (Hash >> 16);

	return 0;
}
}
}
