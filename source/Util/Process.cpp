#include <Util/Process.hpp>

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <TlHelp32.h>

namespace Util
{
namespace Process
{
std::int32_t GetProcessID()
{
	return GetCurrentProcessId();
}

Pointer GetProcessBase()
{
	// Cache base pointer
	static void* CacheBase = nullptr;

	if( CacheBase == nullptr )
	{
		return (CacheBase = GetModuleHandle(nullptr));
	}

	return Pointer(CacheBase);
}

Pointer GetModuleBase(const char* ModuleName)
{
	void* hSnapShot = CreateToolhelp32Snapshot(
		TH32CS_SNAPMODULE,
		GetCurrentProcessId()
	);

	if( hSnapShot == INVALID_HANDLE_VALUE )
	{
		return nullptr;
	}

	MODULEENTRY32 ModuleEntry = {0};
	ModuleEntry.dwSize = sizeof(MODULEENTRY32);
	std::int32_t Ret = Module32First(hSnapShot, &ModuleEntry);
	while( Ret )
	{
		//If module name matches: return it
		if( !std::strcmp(ModuleName, ModuleEntry.szModule) )
		{
			CloseHandle(hSnapShot);
			return reinterpret_cast<void*>(ModuleEntry.modBaseAddr);
		}
		Ret = Module32Next(hSnapShot, &ModuleEntry);
	}
	CloseHandle(hSnapShot);
	return nullptr;
}

void IterateModules(ModuleCallback ModuleProc, std::uint32_t ProcessID)
{
	void* Snapshot = CreateToolhelp32Snapshot(
		TH32CS_SNAPMODULE,
		ProcessID
	);

	if( Snapshot == INVALID_HANDLE_VALUE )
	{
		return;
	}

	MODULEENTRY32 ModuleEntry = {0};
	ModuleEntry.dwSize = sizeof(MODULEENTRY32);
	Module32First(Snapshot, &ModuleEntry);
	do
	{
		bool Continue = ModuleProc(
			ModuleEntry.szModule,
			ModuleEntry.szExePath,
			Pointer(ModuleEntry.modBaseAddr),
			ModuleEntry.modBaseSize
		);
		if( Continue == false )
		{
			break;
		}
	}
	while( Module32Next(Snapshot, &ModuleEntry) );

	CloseHandle(Snapshot);
	return;
}

void IterateReadableMemory(MemoryCallback MemoryProc)
{
	MEMORY_BASIC_INFORMATION MemInfo;

	for( std::uint8_t* i = nullptr;
		(VirtualQuery(i, &MemInfo, sizeof(MEMORY_BASIC_INFORMATION)))
		== sizeof(MEMORY_BASIC_INFORMATION);
		i += MemInfo.RegionSize
	)
	{
		if( MemoryProc(
			Pointer(i),
			MemInfo.RegionSize,
			MemInfo.Type,
			MemInfo.State,
			MemInfo.Protect) == false )
		{
			break;
		}
	}
}
}
}
