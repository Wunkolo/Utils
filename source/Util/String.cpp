#include <string>
#include <algorithm>
#include <vector>
#include <sstream>
#include <functional>
#include <cctype>
#include <codecvt>

#include <Util/String.hpp>

namespace Util
{
namespace String
{
void Replace(std::string& String, char Find, char Replace)
{
	for( std::size_t i = 0; i < String.length(); ++i )
	{
		if( String[i] == Find )
		{
			String[i] = Replace;
		}
	}
}

std::wstring WidenString(const std::string& String)
{
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> utf16Recoder;
	return utf16Recoder.from_bytes(String);
}

std::string ThinString(const std::wstring& String)
{
	std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> utf8Recoder;
	return utf8Recoder.to_bytes(String);
}

std::string ToLower(const std::string& String)
{
	std::string Lowered(String);
	std::transform(Lowered.begin(), Lowered.end(), Lowered.begin(), tolower);
	return Lowered;
}

std::vector<std::string> SplitString(const std::string& String, char Delimiter)
{
	std::vector<std::string> SplitString;
	std::stringstream StringStream(String);
	std::string Token;
	while( std::getline(StringStream, Token, Delimiter) )
	{
		SplitString.push_back(Token);
	}
	return SplitString;
}

// Trim from start
std::string& TrimL(std::string& String)
{
	String.erase(String.begin(), std::find_if(String.begin(), String.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
	return String;
}

// Trim from end
std::string& TrimR(std::string& String)
{
	String.erase(std::find_if(String.rbegin(), String.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), String.end());
	return String;
}

std::string Trim(const std::string& String, bool Reverse)
{
	std::string retValue(String);
	if( Reverse ) // From End
	{
		retValue.erase(std::find_if(retValue.rbegin(), retValue.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), retValue.end());
	}
	else // From Start
	{
		retValue.erase(retValue.begin(), std::find_if(retValue.begin(), retValue.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
	}

	return retValue;
}

std::vector<std::string> Wrap(const std::string& String, std::size_t LineLength)
{
	std::stringstream StringStream;
	StringStream << String;

	std::vector<std::string> Lines;
	while( StringStream.good() )
	{
		std::string CurrentLine;
		std::getline(StringStream, CurrentLine);
		Lines.push_back(CurrentLine);
	}

	std::vector<std::string> WrappedString;

	for( std::size_t i = 0; i < Lines.size(); ++i )
	{
		std::string currentLine(Lines[i]);
		while( currentLine.length() > LineLength )
		{
			std::size_t wordEnd = LineLength;
			while( wordEnd > 0 && !std::isspace(currentLine[wordEnd]) )
				--wordEnd;
			if( wordEnd <= 0 )
				wordEnd = LineLength;

			WrappedString.push_back(currentLine.substr(0, wordEnd));
			currentLine = currentLine.substr(wordEnd, std::string::npos);
		}

		WrappedString.push_back(currentLine);
	}

	return WrappedString;
}

std::size_t Levenshtein(const std::string& Operand1, const std::string& Operand2)
{
	const std::size_t Op1Size = Operand1.size();
	const std::size_t Op2Size = Operand2.size();
	if( Op1Size == 0 )
	{
		return Op2Size;
	}
	if( Op2Size == 0 )
	{
		return Op1Size;
	}

	std::size_t* Costs = new std::size_t[Op2Size + 1];

	for( std::size_t i = 0; i < Op2Size; i++ )
	{
		Costs[i] = i;
	}

	std::size_t i = 0;
	for( std::string::const_iterator Op1Itr = Operand1.begin();
		Op1Itr != Operand1.end();
		++Op1Itr , ++i )
	{
		Costs[0] = i + 1;
		std::size_t Corner = i;

		std::size_t j = 0;
		for( std::string::const_iterator Op2Itr = Operand2.begin();
			Op2Itr != Operand2.end();
			++Op2Itr , ++j )
		{
			std::size_t Upper = Costs[j + 1];
			if( *Op1Itr == *Op2Itr )
			{
				Costs[j + 1] = Corner; // No edits, get previous corner value
			}
			else
			{
				std::size_t t(Upper < Corner ? Upper : Corner);
				Costs[j + 1] = (Costs[j] < t ? Costs[j] : t) + 1;
			}
			Corner = Upper;
		}
	}

	std::size_t Result = Costs[Op2Size];
	delete[] Costs;
	return Result;
}
}
}
